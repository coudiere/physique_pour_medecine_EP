#!/usr/bin/env python
# coding: utf-8

# Modules à utiliser
import numpy as np
import matplotlib.pyplot as plt

# The dipole formula is OK far from the dipole location. Let's truncate
# in a ball a radius r_min around any dipole, in order to heve nicer
# plots
r_min = 0.2
# This r_min has to be adjusted to the size of the domain of interest.

def potential(p=np.array([[1.,0.]]), r_0=np.array([[0.,0.]]), x=0.,y=0.):
    """Computes the potential field V(r) at the point r=[x,y] of the family
    of dipoles p located at points r_0. It is given by the formula:

    V(r) = sum( p.(r-r_0) / |r-r_0|^3 ).

    Input: 

    p : array of size Nx2 of the moments of the dipoles (there are N
    dipoles)

    r_0 : locations of the N dipoles, array of size Nx2 of the
      coordinates x_0,y_0 

    x,y : arrays of coordinates x,y of any size.

    Output:

    V : array of the values of the potential. It has the shape and size
    of x and y.

    """

    # Find the number of dipoles
    N = np.size(p,0)

    # Allocate memory for V, and initialize to 0
    V = np.zeros_like(x)

    rr_min = r_min * np.ones_like(x)
    
    # Loop over the dipoles
    for i in np.arange(N):
        r = np.maximum(np.sqrt( (x-r_0[i,0])**2 + (y-r_0[i,1])**2 ), rr_min)
        V += (p[i,0]*(x-r_0[i,0]) + p[i,1]*(y-r_0[i,1])) / r**3

    return V

def plot_potential_field(ax, p=np.array([[1.,0.]]), r_0=np.array([[0.,0.]]),
                         window=[-1,1,-1,1]):
    """Plot the contour lines of the potential field associated to a finite
    family of dipoles p located at points r_0. The plot is limited to the
    window given as the last argument.

    r_0: array of size Nx2 of the locations of the dipoles, [x1,y1;
    x2,y2...]  

    p: array of size Nx2 of the dipole moments, [p1x,p1y; p2x,p2y...]
    
    window: array of size 4, [xmin,xmax, ymin,ymax]

    ax: Axes objects given as an argument, or returned otherwise

    """

    N = np.size(p,0); # Number of dipoles

    # Potential field
    xmin = window[0]; xmax = window[1];
    ymin = window[2]; ymax = window[3];

    # Number of divisions in each direction for the plot
    Nx = 50;
    Ny = 50;

    # First, create a grid of points x,y. Here, x and y will be matrices
    # such that x_{ji} = x_min + i*hx and y_{ji} = y_min + j*hy, where
    # hx = (x_max-x_min)/Nx, and hy = (y_max-y_min)/Ny
    [x,y] = np.meshgrid(np.linspace(xmin,xmax,Nx+1), np.linspace(ymin,ymax,Ny+1));

    # Now compute the potential field (see potential.m)
    V = potential(p,r_0,x,y);

    # Finally draw the contour lines of the field V
    CS = ax.contour(x,y,V, 26)
    ax.clabel(CS, inline=1, fontsize=10)
    
    # Draws arrows representing the dipoles
    scale = 0.1
    for i in np.arange(N):
        ax.arrow(r_0[i,0],r_0[i,1], scale*p[i,0],scale*p[i,1],
                 head_width = 0.02, head_length = 0.04)

    # Legend and title
    ax.set_title("Potential field")
    ax.set_xlabel('x');
    ax.set_ylabel('y');

    return ax
    
def plot_moving_dipole(p,r, window=[-1,1,-1,1],t_final=1.,N=100,
                       x=np.array([[-1.,-1],[1.,-1]])):
    """Plot the electrical field associated to a single dipole moving along
    a path. The dipole is assumed to be always aligned with the path,
    and with an amplitude given by a function of time.

    p, r: function R-->R^2 that define the dipole vector (p) and its
    location (r) as functions of time.
    
    window: [xmin,xmax,ymin,ymax], window to draw the plot
    
    t_final: final time (starting from 0)
    
    N: number of iterations in time from 0 to t_final
    
    x: coordinates of the points where a potential has to be recorded
       along time, one point per row [x1;x2;x3...]

    """

    t = np.linspace(0,t_final,N+1)
    Nt = np.size(t)
    
    # Initialise the arrays for the V and t
    Nx = np.size(x,0);
    Vs = np.zeros((Nx,Nt));

    # Get the measures
    for i in np.arange(Nt):
        for k in np.arange(Nx):
            Vs[k,i] = potential(p(t[i]),r(t[i]), x[k,0],x[k,1])

    # Prepare the figure and axes for the plot
    fig,(ax1,ax2) = plt.subplots(1,2)
  
    for i in np.arange(Nt):

        # Update the plot of the potential field
        plot_potential_field(ax1, p=p(t[i]),r_0=r(t[i]), window=window)
        ax1.set_aspect('equal')
        # Update the plot of the measures
        for k in np.arange(Nx):
            ax1.plot(x[k,0],x[k,1],'s',markersize=12)
            ax2.plot(t,Vs[k,:],'k:')
            ax2.plot(t[i],Vs[k,i],'s',label='potential at point {}'.format(x[k,:]))
        ax2.set_xlabel('time')
        ax2.set_ylabel('potential')
        ax2.set_title('time t = {:3.1f}'.format(t[i]))
        ax2.legend()
        # Show the figure
        fig.show() # This line makes it not working in a jupyter notebook
        plt.pause(0.02)
        ax1.cla()
        ax2.cla()

    return t,Vs
    
def f(t):
    return np.array([[0.1*t, 0.]])

def df(t):
    return np.array([[0.1,0.]])
    
if __name__ == '__main__':

    p = np.array([[1.,0.], [0.,1.]])
    r = np.array([[0.,0.], [0., 0.2]])
    window = np.array([-1,1,-1,1], dtype=np.float64)
    fig, ax = plt.subplots()
    plot_potential_field(p,r,window, fig,ax)
    plt.show()
