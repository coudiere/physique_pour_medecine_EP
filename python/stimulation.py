#!/usr/bin/env python
# coding: utf-8

# Modules à utiliser
import numpy as np
import matplotlib.pyplot as plt

def rectangular(s):
    """Normalized stimulation: the function f(s) = 1 in (0,1) and 0
    elsewhere.

    """
    return 1.*(0.<s)*(s<1.) # 1 in (0,1), 0 elsewhere

# We assume that there is just 1 stimulation, defined by 
offset   =  10.
duration =   1.
period   = 600.
amplitude=   0.1
time_fun = rectangular

def time_modulation(t):
    """Modulation in time of the stimulation. It is defined by the offset,
    duration, period, amplitude of the time protocol.

    """
    # Normalized time used in the stimulation function
    s = np.mod(t-offset, period) / duration
    return amplitude*time_fun(s)

if __name__ == '__main__':

    # Just show the time curse of the stimulation function
    t0,t1 = 0., offset+3.5*period
    t = np.linspace(t0,t1,10*int(t1-t0)+1)
    plt.plot(t,time_modulation(t), 'b')
    plt.xlabel('t (ms)')
    plt.title('Stimulation protocol in time')
    plt.show()
