import numpy as np
import matplotlib.pyplot as plt
import dipoles

# Mouvement de translation d'un dipôle en diagonal
def r(t):
    return np.array([[-1.+0.1*t,0.]])
def p(t):
    return np.array([[1.,1.]])/np.sqrt(2.)

t,phi = dipoles.plot_moving_dipole(p=p,r=r, t_final=20, N=100)
