#!/usr/bin/env python
# coding: utf-8

# Modules à utiliser
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy.integrate import odeint, ode

import stimulation as stim

# Paramètres types du modèles MS
# ==============================

# Human
tau_in, tau_out, tau_open, tau_close, v_gate = 0.3, 6.0, 120.0, 150.0, 0.13

# Human but short apd
#tau_in, tau_out, tau_open, tau_close, v_gate = 0.3, 6.0,  70.0,  80.0, 0.13

# Rat
#tau_in, tau_out, tau_open, tau_close, v_gate = 0.07,8.4,  25.7,  15.4, 0.02

# Tune the stimulation
stim.offset    =  10.
stim.duration  =   1.
stim.period    = 600.
stim.amplitude =   0.1
stim.time_fun  = stim.rectangular

def f(t,y):
    """Mitchell Schaeffer model with Y = [[u],[h]] an array of size
    2xN_items. The result is the RHS of the ODE, F(t,Y), an array of the
    same size.

    """
    h_infty = 1.*(y[0]<v_gate) + 0.*(y[0]>=v_gate)
    tau = tau_open*(y[0]<v_gate) + tau_close*(y[0]>=v_gate)
    return np.array(
        [ y[1]*(1.-y[0])*(y[0]**2) / tau_in - y[0] / tau_out,
          (h_infty - y[1]) / tau ] )
          
def f_with_stim(t,y):
    """Mitchell Schaeffer model with Y = [[u],[h]] an array of size
    2xN_items. The result is the RHS of the ODE, F(t,Y), an array of the
    same size. The same stimulation term, modulated in time, is added to
    the 1st equation for all items.

    """
    h_infty = 1.*(y[0]<v_gate) + 0.*(y[0]>=v_gate)
    tau = tau_open*(y[0]<v_gate) + tau_close*(y[0]>=v_gate)
    return np.array(
        [ y[1]*(1.-y[0])*(y[0]**2) / tau_in - y[0] / tau_out + stim.time_modulation(t),
          (h_infty - y[1]) / tau ] )
          
def solve(t=np.linspace(0.,100.,10),y0=np.array([0.,1.])):
    """Solve the MS model with initial data y0, model m, on the times t."""
    y = np.zeros((np.size(t),np.size(y0)))
    y[0,:] = y0

    # dopri5, lsoda, etc
    # We want at least 2 time steps during the stimulations
    dt_max = np.min(stim.duration)
    r = ode(f_with_stim).set_integrator('lsoda', max_step=0.5*dt_max)
    r.set_initial_value(y[0,:],t[0])
    for i in np.arange(1,np.size(t)):
        y[i,:] = r.integrate(t[i])
    return y

def plot_phase_plane(ax):
    """Trace la plan de phase du modèle de Mitchell Schaeffer, en montrant
    les nullclines et le flow.

    """
    # En MS on s'intéresse aux valeurs -0.1 < u < 1.1 et -0.1 < h < 1.1
    u = np.linspace(-0.1,1.1,50)
    h = np.linspace(-0.1,1.1,50)
    U,H = np.meshgrid(u,h)
    fU = np.zeros_like(U)
    fH = np.zeros_like(H)
    for i in np.arange(np.size(U,0)):
        y = f(0.,np.vstack((U[i,:],H[i,:])))
        fU[i,:] = y[0]
        fH[i,:] = y[1]

    # Prepare the plot
    ax.set_title('Phase plane, MS model')
    ax.set_xlabel('u')
    ax.set_ylabel('h')
    # Draw the nullcline u'=0, which is u=0 or h = tau_in/tau_out /u/(1-u)
    x = np.linspace(0.01,0.99,250)
    y = tau_in/tau_out / x / (1.-x)
    # First nullcline u'=0
    ax.plot(x, y, 'r')
    # Second nullcline u'=0
    ax.plot(np.array([0.,0.]), np.array([-0.1,1.1]), 'r', label='nullcline du/dt=0')
    # Draw the nullcline h'=0, which is the line h=h_infty
    x = np.array([-0.1,v_gate,v_gate,1.1])
    y = np.array([1.,  1.,    0.,    0.])
    ax.plot(x, y, 'b', label='nullcline dh/dt=0')
    # And the nullcline h'=0, which is
    Nuh = np.sqrt(fU**2 + fH**2)
    Nuh18 = Nuh**(1./4)
    Q = ax.quiver(U,H, Nuh18*fU/Nuh, Nuh18*fH/Nuh, Nuh18, scale=20.)
    #, units='xy', angles='xy')#, headwidth=1, headlength=2)
    # Grid
    ax.grid()
    # limites en x et y
    ax.set_xlim([-0.1,1.1])
    ax.set_ylim([-0.1,1.1])
    ax.legend(loc="upper center")
    
if __name__ == '__main__':

    # Solve the most standard case for human
    t0,t1 = 0., 4000.
    t = np.linspace(t0,t1,np.int32(t1-t0)+1)
    y0 = np.array([0.,1.0])
    y = solve(t,y0)

    # Built the line for the stimulation. The time of activation are the
    # time t = offset + k*period that are less than t1. Hence the maxium
    # value of k is (t1-offset)/period.
    Ns = np.floor_divide(t1-stim.offset, stim.period)
    t_stim = stim.offset + np.arange(Ns+1)*stim.period
    y_stim = 0.*t_stim
    
    plt.plot(t,y[:,0], 'r-', label='u')
    plt.plot(t,y[:,1], 'b:', label='h')
    plt.plot(t_stim,y_stim, 'ks', label='stimulus, {} BPM'.format(60.*1e3/stim.period))
    plt.xlabel('t (ms)')
    plt.ylabel('u,h')
    plt.legend()
    plt.title('MS model with {} parameters'.format("human"))
    plt.show()
